Central Bond Court Disposition Audit Data
=========================================

What is this?
-------------

Monthly Central Bond Court Disposition Audit reports from the Cook County Criminal Court, CSV files extracted from the PDFs and processing code to do the extraction. These reports are sometimes also referred to as "monthly disposition statistcs reports" or "star reports". They are sent to the Office of the President of the Cook County Board of Commissioners by either the Office of the Cook County Clerk of the Circuit Court or the Office of the Chief Judge.

The audit reports were obtained through a public records request to the Office of the President of the Cook County Board of Commissioners.

What's in here?
---------------

* `data/cbc_disposition_stats.csv` - All the data, in one CSV file.
* `data/cbc_disposition_stats__*.csv` - CSV files extracted from the PDFs.
* `data/src/cbc_disposition_stats__*.pdf` - Monthly report PDFs, as obtained from the Office of the President.
* `data/src/cbc_disposition_stats__*.txt` - Monthly report text files, as obtained from the Office of the President. I started receiving this format in May 2018 and the reports have separate files for felony and misdemeanor. Each file contains multiple tables for each [district](http://www.cookcountycourt.org/ABOUTTHECOURT/OrganizationoftheCircuitCourt.aspx).
* `Makefile` - makefile that defines the data processing pipeline.
* `Pipfile` - Defines this project's Python dependencies. See the [pipenv](https://github.com/kennethreitz/pipenv) documentation.
* `Pipfile.lock` - Lock file that specifies precise versions of this project's Python dependencies. See the [pipenv](https://github.com/kennethreitz/pipenv) documentation.
* `processors` - Python helpers for recipies in the makefile.
* `README.md` - You're looking at it.

Data questions
--------------

* When talking about this data, is it more correct to say "people" or "cases"?

Data fields
-----------

### `date`

Date for the other fields in a row of data.

Type: String representing a date.

Acceptable values: Sting representin a date in `%Y-%m-%d` format. In the source data, the dates are formatted in `%m/%d/%Y` format.

Example value: `2017-02-01`

In the source data, this column is named based on the month and year, for example `Apr-17`.

### `Nolle Pros`

Number of cases prosecutors determine will not be prosecuted. "Nolle pros" is a type of dismissal.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example values:

* `0`
* `6`

### `No Bail`

Number of cases where a judge will not set bail, meaning the person is ordered incarcerated in Cook County Jail.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `13`

### `Bond to Stand`

This is when the judge in bond court reaffirms a bond previously set by a different judge, and is most commonly the case for people who were arrested on a warrant that already had a bond attached to it.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `2`

### `I-Bonds`

Number of cases where where the judge issues an "I-bond". With this kind of bond, a person is not required to pay money before being release, but may be required to pay the amount of the bond if they do not appear in court.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `39`

### `EMI`

Number of cases where the judge issues an "I-bond", but with the addition of electronic monitoring. People with these bonds can pay a certain amount of money to get off of electronic monitoring. This number reflects the number of cases where EMI is ordered.

This number reflects typical electronic monitoring.

QUESTION: Is this value a subset of `I-Bonds` or a completely separate category.

QUESTION: What is the relationship between this field and the `Admit to Sheriff EM Prg` and the `Not Admitted to EM Prg`?

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `14`

### `D-Bonds`

Number of cases where the judge issues a "deposit bond" or "D-bond". In these cases, a person must pay ten percent of the bond amount or remain in jail.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `47`

### `C-Bonds`

Number of cases where the judge issues a "cash bond" or "C-bond". In these cases, a person must pay the entire amount of the bond or remain in jail.

### `TOTAL CASES`

Total number of cases in bond court on a given day.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `186`

### `Electronic Monitor`

Number of cases where someone is put on a special alcohol consumption monitoring.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `0`

### `Admit to Sheriff EM Prg`

TODO: Write a description of this field.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `11`

### `Not Admitted to EM Prg`

TODO: Write a description of this field.

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `22`

### `RELS'D ON C BOND`

TODO: Write a description of this field.

QUESTION: The numbers in this field don't seem to line up with the `C-Bonds` field. Is this the number of people who are released on C-bonds, but their case may have come up a previous day in bond court?

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `1`

### `RELS'D ON D BOND`

TODO: Write a description of this field.

QUESTION: The numbers in this field don't seem to line up with the `D-Bonds` field. Is this the number of people who are released on D-bonds, but their case may have come up a previous day in bond court?

Type: Number

Acceptable values: Any integer greater than or equal to zero.

Example value: `104`

Assumptions
-----------

* Python 3
* [pipenv](https://github.com/kennethreitz/pipenv)
* GNU Make
* GNU sed

Installation
------------

Clone the repo for this project:

```
git clone git@gitlab.com:ChicagoDataCooperative/bond-court-dispositions.git
```

Execute the rest of the commands in the project directory created when you cloned the repository:

```
cd bond-court-dispositions
```

Create a Python environment for this project and install dependencies:

```
pipenv install --three
```

Create CSVs from PDFS
---------------------

```
make
```
