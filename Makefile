DATA_DIR = data
DATA_DIR_SRC = $(DATA_DIR)/src
SOURCE_PDF_PATTERN = $(DATA_DIR_SRC)/cbc_disposition_stats__*.pdf
SOURCE_TXT_PATTERN = $(DATA_DIR_SRC)/cbc_disposition_stats__*.txt
SOURCE_PDF_FILES = $(wildcard $(SOURCE_PDF_PATTERN))
SOURCE_TXT_FILES = $(wildcard $(SOURCE_TXT_PATTERN))
OUTPUT_CSV_FILES = $(addprefix $(DATA_DIR)/,$(addsuffix .csv, $(basename $(notdir $(SOURCE_PDF_FILES)))))
OUTPUT_CSV_FROM_TXT_FILES = $(addprefix $(DATA_DIR)/,$(addsuffix .csv, $(basename $(notdir $(SOURCE_TXT_FILES)))))

.PHONY: all clean

all: data/cbc_disposition_stats.csv $(OUTPUT_CSV_FROM_TXT_FILES)

# TODO: Figure out why the `sed` command below doesn't successfully convert
# "Jan-17" to "date".
data/cbc_disposition_stats.csv: $(OUTPUT_CSV_FILES)
	csvstack $? |\
	csvgrep -c 1 -r '[\-]+|.*TOT.*' -i |\
	./processors/csvsortbydate.py -c 1 |\
	sed '1s/^[^,]\+,\(.*\)/date,\1/' \
	> $@

# TODO: Parse text reports

data/cbc_disposition_stats__%__felony.csv: data/src/cbc_disposition_stats__%__felony.txt
	cat $< | ./processors/parse_text_report.py > $@

data/cbc_disposition_stats__%__misdemeanor.csv: data/src/cbc_disposition_stats__%__misdemeanor.txt
	cat $< | ./processors/parse_text_report.py > $@

data/cbc_disposition_stats__%.csv: data/src/cbc_disposition_stats__%.pdf
	./processors/pdf2csv.py $< |\
	./processors/merge_headers.py |\
	./processors/translate_headers.py |\
	./processors/fix_unnamed_columns.py |\
	./processors/fix_dates.py \
	> $@

clean:
	rm data/cbc_disposition_stats.csv
