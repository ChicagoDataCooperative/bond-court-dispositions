#!/usr/bin/env python

"""
Fix the misaligned columns found in the September, 2017 file.

Just pass through other files.

"""

import csv
import sys


def remove_unnamed(row):
    return [
        col for col in row
        if not col.startswith("Unnamed")
    ]


def remove_empty(row):
    return [col for col in row if col != ""]


if __name__ == "__main__":
    reader = csv.reader(sys.stdin)
    writer = csv.writer(sys.stdout)

    removed_cols = False
    for i, row in enumerate(reader):
        if i == 0:
            # Header column
            without_unnamed = remove_unnamed(row)
            if len(without_unnamed) < len(row):
                removed_cols = True

            writer.writerow(without_unnamed)
            continue

        if removed_cols:
            writer.writerow(remove_empty(row))
        else:
            writer.writerow(row)
