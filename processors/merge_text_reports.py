#!/usr/bin/env python

"""
Merge text reports to get a total of all cases.
"""

import argparse
import sys

import pandas as pd


def aggregate_districts(df):
    """Merge all districts into one value per date"""
    return df.groupby('DISP DT').sum().drop('district', 1)


def aggregate_dates(df):
    """Merge multiple rows per date into a single value

    In practice, this will be one row for felony and one for misdemeanor.

    """
    return df.groupby('DISP DT').sum()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge text reports.")
    parser.add_argument('files', nargs='+')
    args = parser.parse_args()

    dfs = []

    for path in args.files:
        df = pd.read_csv(path)
        df_districts_merged = aggregate_districts(df)
        dfs.append(df_districts_merged)

    felony_and_misd = pd.concat(dfs)
    aggregate_dates(felony_and_misd).to_csv(sys.stdout)
