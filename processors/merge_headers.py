#!/usr/bin/env python

"""
Merge header rows

The CSV extracted by Tabula from many files has separate lines for column
eaders that span multiple lines.  Merge these together when neccessary.
Otherwise, just pass the headers through unchanged.

"""

import csv
import sys


def should_merge_headers(header_rows):
    if header_rows[1][1].strip() == '':
        # The first column of the second row is empty. We should merge the
        # headers.
        return True

    if header_rows[1][0].strip() == "CBC STATS":
        return True

    return False


def remove_unnamed(s):
    """Convert strings like 'Unnamed: 1' to an empty string"""
    if s.startswith("Unnamed:"):
        return ""

    return s


def normalize_codes(s):
    """Convert floating point code to integer string"""
    return s.rstrip(".0")


def merge_headers(header_rows, sep=" ", skip_cols=[]):
    """Merge header rows into one"""
    skip_cols_set = set(skip_cols)
    output_header = []

    for i, row in enumerate(header_rows):
        for j in range(len(row)):
            if i == 0:
                # First header row.  Copy the value
                output_header.append(remove_unnamed(row[j]))
                continue

            if j in skip_cols_set:
                # We're not merging this column. Skip it
                continue

            if not row[j]:
                # Empty value, don't merge
                continue

            # Merge the values!
            output_header[j] = "{}{}{}".format(
                    output_header[j], sep, normalize_codes(row[j])).strip()

    return output_header


def is_header_row(i, row):
    # TODO: Parameterize this
    # The indices of rows we want to merge
    header_indices = set([0, 1])

    if i in header_indices:
        return True

    if i == 2 and row[0] == "Stats":
        # HACK: Special case for the February, 2018 file, which has 3 header
        # rows.
        return True

    return False


if __name__ == "__main__":
    reader = csv.reader(sys.stdin)
    writer = csv.writer(sys.stdout)

    in_header = True

    header_rows = []
    for i, row in enumerate(reader):
        if in_header:
            if is_header_row(i, row):
                header_rows.append(row)
                continue

            # We've consumed all the header rows.
            # Merge them, output them, and continue
            if should_merge_headers(header_rows):
                writer.writerow(merge_headers(header_rows))

            else:
                for hr in header_rows:
                    writer.writerow(hr)

            in_header = False

        # Not a header row. Just output it.
        writer.writerow(row)
