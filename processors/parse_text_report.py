#!/usr/bin/env python

"""
Parse a text report

Starting in May 2018, reports are in a text format instead of PDF and there are
multiple tables per file.

"""

import csv
import re
import sys

from transitions import Machine


class LineBuffer:
    """
    Convenience wrapper for lines of a text file

    This is needed because when parsing text line-by-line it's often useful to
    look forward or backward in the file.

    """
    def __init__(self, f):
        self._f = f
        self._current_line_idx = None
        self._lines = []
        self.has_lines = True

    def __iter__(self):
        return self

    def __next__(self):
        if self._current_line_idx is None:
            self._current_line_idx = 0
        else:
            self._current_line_idx += 1

        try:
            line = next(self._f)
            self._lines.append(line)

            return line

        except StopIteration:
            self.has_lines = False
            raise

    @property
    def line_number(self):
        return self._current_line_idx + 1

    @property
    def current_line(self):
        if self._current_line_idx is None:
            return None

        return self._lines[self._current_line_idx]

    def __getitem__(self, item):
        return self._lines[item]


class TextReportParser(object):
    """Parser for reports after May 2018"""

    states = [
        'page',
        'header',
        'table_header',
        'table_body',
        'table_footer',
    ]

    def __init__(self, f):
        self._lines = LineBuffer(f)
        self._report_type = None
        self._district = None
        self._rows = []

        self._machine = Machine(
            model=self,
            states=self.states,
            initial='page',
            send_event=True
        )

        self._machine.add_transition(
            'start_header',
            ['page', 'table_footer'],
            'header'
        )

        self._machine.add_transition(
            'start_table_header',
            'header',
            'table_header'
        )

        self._machine.add_transition(
            'start_table_body',
            'table_header',
            'table_body'
        )

        self._machine.add_transition(
            'start_table_footer',
            'table_body',
            'table_footer'
        )

    def parse(self):
        while self._lines.has_lines:
            self.handle_state(self._lines)

        return self._rows

    def handle_state(self, lines):
        handler_name = 'handle_state_{state}'.format(state=self.state)
        handler = getattr(self, handler_name)
        handler(lines)

    def handle_state_page(self, lines):
        for line in lines:
            if "CIRCUIT COURT OF COOK COUNTY" in line:
                self.start_header()
                return

    def handle_state_header(self, lines):
        for line in lines:
            if "MISDEMEANOR" in line:
                self._report_type = "misdemeanor"

            elif "FELONY" in line:
                self._report_type = "felony"

            m = re.search(r'DISTRICT (?P<district_num>\d)', line)

            if m is not None:
                self._district = m.group('district_num')

            if re.match(r'\s+-{5,}', line):
                self.start_table_header()
                return

    def handle_state_table_header(self, lines):
        for line in lines:
            if re.match(r'\s*-{3,}', line):
                self.start_table_body()
                return

    def handle_state_table_body(self, lines):
        for line in lines:
            if re.match(r'\s*-{5,}', line):
                self.start_table_footer()
                return

            if re.match(r'\s*\d{2}/\d{2}/\d{2}', line):
                self._rows.append(self._parse_row(line))

    def handle_state_table_footer(self, lines):
        for line in lines:
            if "CIRCUIT COURT OF COOK COUNTY" in line:
                self.start_header()
                return

    def _parse_row(self, line):
        row = re.split(r'\s{2,}', line)

        if row[0] == "":
            row = row[1:]

        if row[-1] == "":
            row = row[:-1]

        return row + [self._district, self._report_type]


if __name__ == "__main__":
    cols = [
        'DISP DT',
        '103',
        '606',
        '604',
        '605',
        '277',
        '601',
        '153',
        'TOTALS',
        '894',
        '278',
        '279',
        'RLS ON C',
        'RLS ON D',
        'district',
        'report_type',
    ]

    parser = TextReportParser(sys.stdin)
    rows = parser.parse()

    writer = csv.writer(sys.stdout)
    
    writer.writerow(cols)

    for row in rows:
        writer.writerow(row)
