#!/usr/bin/env python

"""
Convert header labels from numeric values to human readable ones.

The footers of the PDFs have a translation between the headers and
their meanings.  Make the headers in the output file easier for
humans to read.

"""

import csv
import sys

HEADER_CODES = {
    '103': "Nolle Pros",
    '606': "No Bail",
    '604': "Bond to Stand",
    '605': "I-Bonds",
    '277': "EMI",
    '601': "D-Bonds",
    '153': "C-Bonds",
    '894': "Electronic Monitor",
    '278': "Admit to Sheriff EM Prg",
    '279': "Not Admitted to EM Prg",
}


def translate_values(row, lookup):
    return [lookup[x] if x in lookup else x for x in row]


if __name__ == "__main__":
    # TODO: Parameterize this
    header_index = 0

    reader = csv.reader(sys.stdin)
    writer = csv.writer(sys.stdout)

    for i, row in enumerate(reader):
        if i == header_index:
            writer.writerow(translate_values(row, HEADER_CODES))
        else:
            writer.writerow(row)
