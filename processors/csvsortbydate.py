#!/usr/bin/env python

import agate
from csvkit.cli import parse_column_identifiers
from csvkit.utilities.csvsort import CSVSort


class CSVSortByDate(CSVSort):
    def main(self):
        if self.args.names_only:
            self.print_column_names()
            return

        table = agate.Table.from_csv(
            self.input_file,
            skip_lines=self.args.skip_lines,
            sniff_limit=self.args.sniff_limit,
            column_types=self.get_column_types(),
            **self.reader_kwargs
        )

        column_ids = parse_column_identifiers(
            self.args.columns,
            table.column_names,
            self.get_column_offset()
        )
        sorter = self.get_sorter(column_ids)

        table = table.order_by(sorter, reverse=self.args.reverse)
        table.to_csv(self.output_file, **self.writer_kwargs)

    def get_sorter(self, column_ids):
        def sorter(row):
            datestrs = []
            for column_id in column_ids:
                try:
                    dt = row[column_id]
                    datestrs.append(dt.strftime("%Y-%m-%d"))
                except ValueError:
                    print(row[column_id])

            return "".join(datestrs)

        return sorter


def launch_new_instance():
    utility = CSVSortByDate()
    utility.run()


if __name__ == '__main__':
    launch_new_instance()
