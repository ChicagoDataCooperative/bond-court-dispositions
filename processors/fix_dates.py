#!/usr/bin/env python

"""
Fix badly formed dates.
"""

import csv
import sys


def fix_date(row):
    if row[0] == '011/2017':
        row[0] = '1/1/2017'

    return row


if __name__ == "__main__":
    reader = csv.reader(sys.stdin)
    writer = csv.writer(sys.stdout)

    for row in reader:
        writer.writerow(fix_date(row))
